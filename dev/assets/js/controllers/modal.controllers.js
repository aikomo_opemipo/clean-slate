angular.module('modal.controllers', [])
    .controller('HeaderCtrl', ['$state', '$scope', 'API', function($state, $scope, $API) {
        $scope.search = {};
        $scope.parameter = 'customers';

        $scope.runSearch = function() {
            $scope.searching = true;
            $scope.search.results = false;
            setTimeout(function() {
                $scope.searching = false;
                $scope.search.results = true;
                $scope.$apply()
            }, 1000);
        }

        $scope.searchFilter = function(param) {
            $scope.parameter = param;
            $scope.runSearch()
        }
    }]);
