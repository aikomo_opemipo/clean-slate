angular.module('app.authService', [])
    .factory('Auth', ['$http', 'LocalService', 'API', '$q', '$state',
        function ($http, LocalService, $API, $q, $state) {
            return {
                authorize: function(event, state, params) {
                    var deferred = $q.defer();
                    var authFactory = this;

                    if (authFactory.isAuthenticable(state) && state.redirectTo == null) {
                        var current_session = LocalService.get('current_session');
                        if (!current_session) {
                            event.preventDefault();
                            $state.go('access.login');
                        }
                    } else {
                        var stateName = state.name.split('.');
                        if (_.contains(stateName, 'access') && authFactory.isLoggedIn()) {
                            $state.go('app.dashboard');
                        }
                    }
                },
                isAuthenticable: function(state) {
                    return state.data && state.data.authenticable
                },
                isLoggedIn: function() {
                    return LocalService.get('current_session') != null
                },
                login: function (credentials) {
                    var deferred = $q.defer();
                    $API.one('login').get(credentials).then(function (data) {
                        LocalService.set('current_session', JSON.stringify(data));
                        deferred.resolve(data);
                    }, function (error) {
                        deferred.reject(error);
                    });

                    return deferred.promise;
                },
                logout: function () {
                    LocalService.unset('current_session');
                },
                getUser: function () {
                    if (LocalService.get('current_session')) {
                        return angular.fromJson(LocalService.get('current_session')).user;
                    } else {
                        return false;
                    }
                },
                getToken: function() {
                    if (LocalService.get('current_session')) {
                        return angular.fromJson(LocalService.get('current_session')).token;
                    } else {
                        return false;
                    }
                }
            }
        }
    ])
    .factory('AuthInterceptor', ['$q', '$injector', 'LocalService',
        function ($q, $injector, LocalService) {
            return {
                responseError: function (response) {
                    if (response.status === 401 || response.status === 403) {
                        LocalService.unset('current_session');
                        $injector.get('$state').go('access.signin');
                    }
                    return $q.reject(response);
                }
            }
        }
    ])
    .config(['$httpProvider',
        function ($httpProvider) {
            $httpProvider.interceptors.push('AuthInterceptor');
        }
    ]);