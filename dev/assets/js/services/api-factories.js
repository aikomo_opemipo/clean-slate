angular.module('app.apiFactories', ['restangular'])
    .factory('API', ['Restangular', 'DEFAULTS', function(Restangular, DEFAULTS) {
        return Restangular.withConfig(function(RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl(DEFAULTS.baseURL);
        });
    }])
    .factory('DataAPI', ['Restangular', 'DEFAULTS', 'Auth', function(Restangular, DEFAULTS, Auth) {
        return Restangular.withConfig(function(RestangularConfigurer) {
            RestangularConfigurer.setBaseUrl(DEFAULTS.liveURL);
            RestangularConfigurer.setDefaultHeaders({
                Authorization: 'Bearer ' + Auth.getToken()
            })
        });
    }])
