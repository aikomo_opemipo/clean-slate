angular.module('app.dashboard', ['ui.router'])

.config(['$stateProvider',
    function ($stateProvider) {
        $stateProvider
            .state('app.dashboard', {
                url: '/home',
                templateUrl: 'modules/dashboard/index.html'
            })
    }
])

.controller('DashboardCtrl', ['$scope', '$state', 'Restangular',
    function ($scope, $state, Restangular) {
    }
]);