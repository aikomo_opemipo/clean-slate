angular.module('app.access', ['ui.router'])

.config(['$stateProvider',
    function ($stateProvider) {
        $stateProvider
            .state('access', {
                url: '/access',
                template: '<div ui-view class="fade-in-up smooth"></div>'
            })
            .state('access.login', {
                url: '/login',
                templateUrl: 'modules/access/login.html'
            })
            .state('access.forgot_password', {
                url: '/forgot',
                templateUrl: 'modules/access/forgot_password.html'
            })
    }
])

.controller('LoginCtrl', ['$scope', '$state', 'Auth', 'Notification',
    function ($scope, $state, Auth, Notification) {
        $scope.credentials = {};
        $scope.login = function () {
            Auth.login($scope.credentials).then(function (res) {
                $state.go('app.dashboard');
            }, function (error) {
                Notification.error("Could not login to app", error);
            });
        }
    }
])

.controller('ForgotPasswordCtrl', ['$scope', '$http', '$state', 'API',
    function ($scope, $http, $state, $API) {
    }
]);